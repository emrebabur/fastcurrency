package com.emrebabur.fastcurrency

import com.emrebabur.fastcurrency.application.wrap
import com.emrebabur.fastcurrency.converter.BasicConverterAutomaton
import com.emrebabur.fastcurrency.converter.CURRENCY_DECIMAL_SCALE
import com.emrebabur.fastcurrency.converter.CURRENCY_ROUNDING_MODE
import com.emrebabur.fastcurrency.converter.ConverterAutomaton.State
import com.emrebabur.fastcurrency.converter.ConverterAutomaton.Wish
import com.emrebabur.fastcurrency.converter.Currency
import com.emrebabur.fastcurrency.converter.model.CurrencyRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal

class BasicConverterAutomatonTest {

    private val testScheduler = TestScheduler()
    private val currencyRepository: CurrencyRepository = mock()

    @Test
    fun `init wish should cause new state with defined base currency and others from repository`() {
        whenever(currencyRepository.getCurrencies(any())).thenReturn(Single.just(getRates()))
        val baseCurrency = Currency("BASE", BigDecimal("1"), BigDecimal("1"))
        val automaton = BasicConverterAutomaton(
            currencyRepository = currencyRepository,
            computationScheduler = testScheduler
        )
        val testObserver = automaton.wrap().test()
        val wish = Wish.Init(baseCurrency.name)
        val expectedStates = listOf(
            State.Loading,
            createDisplayingState(
                true, baseCurrency,
                getRates()
            )
        )

        automaton.accept(wish)
        testScheduler.triggerActions()

        assertEquals(
            expectedStates, testObserver.values()
        )
    }

    @Test
    fun `should update rates and amounts`() {
        val rates = getRates(2)
        whenever(currencyRepository.getCurrencies(any())).thenReturn(Single.just(rates))
        val baseCurrency = createCurrency("BASE", BigDecimal("1"), BigDecimal("1"))
        val initialState = createDisplayingState(true, baseCurrency, getRates())
        val automaton = BasicConverterAutomaton(
            currencyRepository = currencyRepository,
            initialState = initialState,
            computationScheduler = testScheduler
        )
        val testObserver = automaton.wrap().test()
        val expectedStates = listOf(
            initialState,
            createDisplayingState(false, baseCurrency, rates)
        )

        val wish = Wish.Init(baseCurrency.name)
        automaton.accept(wish)
        testScheduler.triggerActions()

        assertEquals(
            expectedStates, testObserver.values()
        )
    }

    @Test
    fun `should update base currency`() {
        val rates = getRates()
        whenever(currencyRepository.getCurrencies(any())).thenReturn(Single.just(rates))
        val baseCurrency = createCurrency("BASE", BigDecimal("1"), BigDecimal("1"))
        val newBaseCurrencyName = getRates().keys.last()
        val newBaseCurrencyRate = getRates().getValue(newBaseCurrencyName)
        val newBaseCurrencyAmount =
            newBaseCurrencyRate.divide(baseCurrency.rate).multiply(baseCurrency.amount)
        val newBaseCurrency = createCurrency(
            newBaseCurrencyName,
            newBaseCurrencyRate,
            newBaseCurrencyAmount
        )

        val initialState = createDisplayingState(true, baseCurrency, getRates())
        val automaton = BasicConverterAutomaton(
            currencyRepository = currencyRepository,
            initialState = initialState,
            computationScheduler = testScheduler
        )
        val testObserver = automaton.wrap().test()
        val expectedStates = listOf(
            initialState,
            initialState.copy(
                baseCurrency = newBaseCurrency,
                currencies = initialState.currencies.let {
                    val newList = it.toMutableList()
                    newList.remove(newBaseCurrency)
                    newList.add(0, baseCurrency)
                    newList
                }
            )
        )

        val wish = Wish.ChangeBaseCurrency(newBaseCurrency.name)
        automaton.accept(wish)

        assertEquals(
            expectedStates, testObserver.values()
        )
    }

    @Test
    fun `should update amounts`() {
        val rates = getRates()
        whenever(currencyRepository.getCurrencies(any())).thenReturn(Single.just(rates))

        val baseCurrency = createCurrency("BASE", BigDecimal("1"), BigDecimal("1"))
        val newBaseCurrency = createCurrency("BASE", BigDecimal("1"), BigDecimal("2"))
        val initialState = createDisplayingState(true, baseCurrency, getRates())
        val automaton = BasicConverterAutomaton(
            currencyRepository = currencyRepository,
            initialState = initialState,
            computationScheduler = testScheduler
        )
        val testObserver = automaton.wrap().test()
        val expectedStates = listOf(
            initialState,
            createDisplayingState(false, newBaseCurrency, rates)
        )

        val wish = Wish.ChangeBaseAmount(newBaseCurrency.amount)
        automaton.accept(wish)

        assertEquals(
            expectedStates, testObserver.values()
        )
    }

    private fun getRates(multiplier: Int = 1) = mapOf(
        "AUD" to BigDecimal("1").multiply(BigDecimal(multiplier.toString())),
        "USD" to BigDecimal("2").multiply(BigDecimal(multiplier.toString())),
        "TRY" to BigDecimal("3").multiply(BigDecimal(multiplier.toString())),
        "GBP" to BigDecimal("4").multiply(BigDecimal(multiplier.toString())),
        "EUR" to BigDecimal("5").multiply(BigDecimal(multiplier.toString()))
    )

    private fun createCurrency(name: String, rate: BigDecimal, amount: BigDecimal) =
        Currency(
            name,
            rate.setScale(CURRENCY_DECIMAL_SCALE, CURRENCY_ROUNDING_MODE),
            amount.setScale(CURRENCY_DECIMAL_SCALE, CURRENCY_ROUNDING_MODE)
        )

    private fun createDisplayingState(
        shouldScrollTop: Boolean,
        baseCurrency: Currency,
        rates: Map<String, BigDecimal>
    ) =
        State.Displaying(
            shouldScrollTop = shouldScrollTop,
            baseCurrency = baseCurrency,
            currencies = rates.toList()
                .map {
                    createCurrency(
                        it.first,
                        it.second,
                        it.second.divide(baseCurrency.rate).multiply(baseCurrency.amount)
                    )
                }

        )
}
