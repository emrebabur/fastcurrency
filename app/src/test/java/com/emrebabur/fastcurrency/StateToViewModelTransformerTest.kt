package com.emrebabur.fastcurrency

import com.emrebabur.fastcurrency.converter.ConverterAutomaton.State
import com.emrebabur.fastcurrency.converter.Currency
import com.emrebabur.fastcurrency.converter.view.ConverterViewModel.ViewModel
import com.emrebabur.fastcurrency.converter.view.CurrencyViewModel
import com.emrebabur.fastcurrency.converter.view.StateToViewModelTransformer
import org.junit.Assert.assertEquals
import org.junit.Test
import java.math.BigDecimal


class StateToViewModelTransformerTest {

    private val transformer = StateToViewModelTransformer

    @Test
    fun `should transform loading state to loading view model`() {
        val state = State.Loading
        val expectedViewModel = ViewModel.Loading

        val viewModel = transformer.invoke(state)

        assertEquals(expectedViewModel, viewModel)
    }

    @Test
    fun `should transform displaying state to displaying view model`() {
        val state = State.Displaying(
            shouldScrollTop = false,
            baseCurrency = Currency("test base currency", BigDecimal("1"), BigDecimal("1")),
            currencies = listOf(
                Currency("test currency 1", BigDecimal("1"), BigDecimal("1")),
                Currency("test currency 2", BigDecimal("2"), BigDecimal("2"))
            )
        )

        val expectedViewModel = ViewModel.Displaying(
            shouldScrollTop = false,
            currencies = listOf(
                CurrencyViewModel("test base currency", "1"),
                CurrencyViewModel("test currency 1", "1"),
                CurrencyViewModel("test currency 2", "2")
            )
        )

        val viewModel = transformer.invoke(state)

        assertEquals(expectedViewModel, viewModel)
    }
}
