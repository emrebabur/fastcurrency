package com.emrebabur.fastcurrency.automaton

import com.emrebabur.fastcurrency.application.plusAssign
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class AbstractAutomaton<State, Wish, Effect>(
    initialState: State,
    private val actor: Automaton.Actor<State, Wish, Effect>,
    private val reducer: Automaton.Reducer<State, Effect>,
    private val disposable: CompositeDisposable = CompositeDisposable()
) : Automaton<State, Wish>, Disposable by disposable {

    private val wishRelay = PublishRelay.create<Wish>()
    private val stateRelay = BehaviorRelay.createDefault<State>(initialState)
    private val currentState: State
        get() = stateRelay.value!!

    init {
        disposable += wishRelay.flatMap {
            actor.invoke(currentState, it)
        }
            .map {
                reducer.invoke(currentState, it)
            }
            .subscribe(stateRelay)
    }

    override fun accept(wish: Wish) {
        wishRelay.accept(wish)
    }

    override fun subscribe(observer: Observer<in State>) {
        stateRelay.subscribe(observer)
    }
}
