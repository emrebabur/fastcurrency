package com.emrebabur.fastcurrency.automaton

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer

interface Automaton<State, Wish> : Consumer<Wish>, ObservableSource<State>, Disposable {

    interface Actor<State, Wish, Effect> : (State, Wish) -> Observable<out Effect>

    interface Reducer<State, Effect> : (State, Effect) -> State
}
