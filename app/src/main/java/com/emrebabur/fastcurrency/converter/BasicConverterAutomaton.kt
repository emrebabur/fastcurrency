package com.emrebabur.fastcurrency.converter

import com.emrebabur.fastcurrency.application.ConnectivityHelper
import com.emrebabur.fastcurrency.automaton.AbstractAutomaton
import com.emrebabur.fastcurrency.automaton.Automaton
import com.emrebabur.fastcurrency.converter.ConverterAutomaton.Effect
import com.emrebabur.fastcurrency.converter.ConverterAutomaton.State
import com.emrebabur.fastcurrency.converter.model.CurrencyRepository
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import java.math.BigDecimal
import java.util.concurrent.TimeUnit

class BasicConverterAutomaton(
    currencyRepository: CurrencyRepository,
    connectivityHelper: ConnectivityHelper,
    initialState: State = State.Loading,
    computationScheduler: Scheduler = Schedulers.computation()
) : AbstractAutomaton<State, ConverterAutomaton.Wish, Effect>(
    initialState,
    Actor(initialState, currencyRepository, connectivityHelper, computationScheduler),
    Reducer()
), ConverterAutomaton {

    class Actor(
        private val initialState: State,
        private val currencyRepository: CurrencyRepository,
        private val connectivityHelper: ConnectivityHelper,
        private val computationScheduler: Scheduler
    ) : Automaton.Actor<State, ConverterAutomaton.Wish, Effect> {

        private var baseCurrencyName: String? = null

        override fun invoke(state: State, wish: ConverterAutomaton.Wish): Observable<out Effect> =
            when (wish) {
                is ConverterAutomaton.Wish.Init -> init(state, wish.baseCurrencyName)
                is ConverterAutomaton.Wish.ChangeBaseAmount -> updateBaseAmount(wish.amount)
                is ConverterAutomaton.Wish.ChangeBaseCurrency -> updateBaseCurrency(wish.currencyName)
            }

        private fun init(state: State, baseCurrencyName: String) =
            if (state == initialState) {
                Observable.interval(
                    0,
                    CURRENCY_UPDATE_INTERVAL_MS,
                    TimeUnit.MILLISECONDS,
                    computationScheduler
                )
                    .filter { connectivityHelper.isConnected() }
                    .concatMapEager {
                        currencyRepository.getCurrencies(this.baseCurrencyName ?: baseCurrencyName)
                            .timeout(
                                CURRENCY_UPDATE_TIMEOUT_MS,
                                TimeUnit.MILLISECONDS,
                                computationScheduler
                            )
                            .toObservable()
                    }
                    .retry()
                    .map {
                        Effect.RatesUpdated(
                            baseCurrencyName,
                            it
                        )
                    }
            } else {
                Observable.empty()
            }

        private fun updateBaseAmount(amount: BigDecimal) =
            Observable.just(
                Effect.BaseAmountChanged(amount)
            )

        private fun updateBaseCurrency(name: String) =
            Observable.just(Effect.BaseCurrencyChanged(name)).also {
                baseCurrencyName = name
            }
    }

    class Reducer : Automaton.Reducer<State, Effect> {

        companion object {
            private val ROUNDING_MODE = CURRENCY_ROUNDING_MODE
            private const val SCALE = CURRENCY_DECIMAL_SCALE
        }

        override fun invoke(state: State, effect: Effect): State =
            when (effect) {
                is Effect.RatesUpdated -> updateRates(state, effect)
                is Effect.BaseAmountChanged -> updateAmounts(state, effect)
                is Effect.BaseCurrencyChanged -> changeBaseCurrency(state, effect)
            }

        private fun updateRates(state: State, effect: Effect.RatesUpdated) =
            if (state is State.Displaying) {
                state.copy(
                    shouldScrollTop = false,
                    baseCurrency = state.baseCurrency.copy(
                        rate = BigDecimal("1")
                            .setScale(SCALE, ROUNDING_MODE)
                    ),
                    currencies = state.currencies.map {
                        val rate = (effect.rates[it.name] ?: it.rate)
                        it.copy(
                            rate = rate.setScale(SCALE, ROUNDING_MODE),
                            amount = rate.multiply(state.baseCurrency.amount)
                                .setScale(SCALE, ROUNDING_MODE)
                        )
                    }
                )
            } else {
                State.Displaying(
                    shouldScrollTop = true,
                    baseCurrency = Currency(
                        effect.baseCurrencyName,
                        BigDecimal("1"),
                        BigDecimal(DEFAULT_BASE_CURRENCY_AMOUNT.toString())
                    ),
                    currencies = effect.rates
                        .toList()
                        .map {
                            Currency(
                                name = it.first,
                                rate = it.second
                                    .setScale(SCALE, ROUNDING_MODE),
                                amount = it.second
                                    .setScale(SCALE, ROUNDING_MODE)
                            )
                        }
                )
            }

        private fun updateAmounts(state: State, effect: Effect.BaseAmountChanged) =
            if (state is State.Displaying) {
                state.copy(
                    shouldScrollTop = false,
                    baseCurrency = state.baseCurrency.copy(amount = effect.amount),
                    currencies = state.currencies.map {
                        it.copy(
                            amount = it.rate
                                .divide(
                                    state.baseCurrency.rate,
                                    SCALE,
                                    ROUNDING_MODE
                                )
                                .multiply(effect.amount)
                                .setScale(SCALE, ROUNDING_MODE)
                        )
                    }
                )
            } else {
                state
            }

        private fun changeBaseCurrency(state: State, effect: Effect.BaseCurrencyChanged) =
            if (state is State.Displaying && effect.name != state.baseCurrency.name) {
                val newBaseCurrency = state.currencies
                    .first {
                        it.name == effect.name
                    }
                val newCurrencies = mutableListOf(state.baseCurrency)
                newCurrencies.addAll(
                    state.currencies
                        .filter {
                            it.name != effect.name
                        }
                )

                State.Displaying(
                    shouldScrollTop = true,
                    baseCurrency = newBaseCurrency,
                    currencies = newCurrencies
                )
            } else {
                state
            }
    }
}
