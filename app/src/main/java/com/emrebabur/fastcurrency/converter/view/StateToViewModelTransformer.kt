package com.emrebabur.fastcurrency.converter.view

import com.emrebabur.fastcurrency.converter.ConverterAutomaton

object StateToViewModelTransformer : (ConverterAutomaton.State) -> ConverterViewModel.ViewModel {

    override fun invoke(from: ConverterAutomaton.State): ConverterViewModel.ViewModel =
        when (from) {
            is ConverterAutomaton.State.Loading -> ConverterViewModel.ViewModel.Loading
            is ConverterAutomaton.State.Displaying -> ConverterViewModel.ViewModel.Displaying(
                from.shouldScrollTop,
                from.currencies
                    .toMutableList()
                    .apply {
                        add(0, from.baseCurrency)
                    }
                    .map { CurrencyViewModel(it.name, it.amount.toString()) }
            )
        }
}
