package com.emrebabur.fastcurrency.converter.model

import io.reactivex.Single
import java.math.BigDecimal

interface CurrencyRepository {
    fun getCurrencies(base: String): Single<Map<String, BigDecimal>>
}

class BasicCurrencyRepository(
    private val api: CurrencyApi
) : CurrencyRepository {
    override fun getCurrencies(base: String): Single<Map<String, BigDecimal>> =
        api.getCurrencies(base)
            .map { it.rates }
}
