package com.emrebabur.fastcurrency.converter.view

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.emrebabur.fastcurrency.R
import com.emrebabur.fastcurrency.application.inflate
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.functions.Consumer

class CurrencyViewModelAdapter(
    private val eventRelay: PublishRelay<ConverterEvent>
) : Consumer<List<CurrencyViewModel>>,
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<CurrencyViewModel> = emptyList()

    override fun accept(newItems: List<CurrencyViewModel>) {

        if(items.isEmpty() && newItems.isNotEmpty()) {
            items = newItems
            notifyDataSetChanged()
        } else {
            val baseCurrencyChanged = items.first().name != newItems.first().name
            items = newItems
            if(baseCurrencyChanged)
                notifyItemChanged(0)
            notifyItemRangeChanged(1, items.count()-1)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder =
        when (viewType) {
            ViewType.BASE_CURRENCY.ordinal -> BaseCurrencyViewModelViewHolder(
                parent.inflate(R.layout.base_currency),
                eventRelay
            )
            ViewType.CURRENCY.ordinal -> CurrencyViewModelViewHolder(
                parent.inflate(R.layout.currency),
                eventRelay
            )

            else -> throw IllegalArgumentException("Unknown view type")
        }

    override fun getItemCount(): Int = items.count()

    override fun getItemViewType(position: Int): Int =
        if (position == 0) ViewType.BASE_CURRENCY.ordinal else ViewType.CURRENCY.ordinal

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is BaseCurrencyViewModelViewHolder -> holder.model = items[position]
            is CurrencyViewModelViewHolder -> holder.model = items[position]

            else -> throw IllegalArgumentException("Unknown viewholder type")
        }
    }

    enum class ViewType {
        BASE_CURRENCY, CURRENCY
    }
}
