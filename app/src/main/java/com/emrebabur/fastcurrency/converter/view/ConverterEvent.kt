package com.emrebabur.fastcurrency.converter.view

sealed class ConverterEvent {

    data class ItemClicked(val name: String) : ConverterEvent()
    data class EditTextChanged(val text: String) : ConverterEvent()
}
