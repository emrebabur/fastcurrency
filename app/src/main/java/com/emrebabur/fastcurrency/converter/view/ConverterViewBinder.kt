package com.emrebabur.fastcurrency.converter.view

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import com.emrebabur.fastcurrency.converter.view.ConverterViewModel.ViewModel
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.functions.Consumer

class ConverterViewBinder(
    private val recyclerView: RecyclerView,
    private val loadingView: ProgressBar
) : Consumer<ViewModel>, ObservableSource<ConverterEvent> {

    private val eventRelay = PublishRelay.create<ConverterEvent>()
    private val adapter = CurrencyViewModelAdapter(eventRelay)
    private val layoutManager =
        LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)

    init {
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = null
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    override fun accept(viewModel: ViewModel) {
        when (viewModel) {
            ViewModel.Loading -> showLoading()
            is ViewModel.Displaying -> showConverter(viewModel)
        }
    }

    override fun subscribe(observer: Observer<in ConverterEvent>) {
        eventRelay.subscribe(observer)
    }

    private fun showLoading() {
        recyclerView.visibility = View.INVISIBLE
        loadingView.visibility = View.VISIBLE
    }

    private fun showConverter(viewModel: ViewModel.Displaying) {
        recyclerView.visibility = View.VISIBLE
        loadingView.visibility = View.INVISIBLE
        adapter.accept(viewModel.currencies)
        if(viewModel.shouldScrollTop) recyclerView.scrollToPosition(0)
    }
}
