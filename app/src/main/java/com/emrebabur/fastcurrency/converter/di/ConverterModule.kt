package com.emrebabur.fastcurrency.converter.di

import com.emrebabur.fastcurrency.application.ConnectivityHelper
import com.emrebabur.fastcurrency.converter.BasicConverterAutomaton
import com.emrebabur.fastcurrency.converter.ConverterAutomaton
import com.emrebabur.fastcurrency.converter.model.BasicCurrencyRepository
import com.emrebabur.fastcurrency.converter.model.CurrencyApi
import com.emrebabur.fastcurrency.converter.model.CurrencyRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ConverterScope

private const val BASE_URL = "https://revolut.duckdns.org/"

@Module
class ConverterModule {

    @Provides
    @ConverterScope
    fun currencyApi(builder: Retrofit.Builder): CurrencyApi =
        builder.baseUrl(BASE_URL)
            .build()
            .create(CurrencyApi::class.java)

    @Provides
    @ConverterScope
    fun currencyRepository(api: CurrencyApi): CurrencyRepository =
        BasicCurrencyRepository(api)

    @Provides
    @ConverterScope
    fun converterAutomaton(
        currencyRepository: CurrencyRepository,
        connectivityHelper: ConnectivityHelper
    ): ConverterAutomaton =
        BasicConverterAutomaton(currencyRepository, connectivityHelper)
}
