package com.emrebabur.fastcurrency.converter.model

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("latest")
    fun getCurrencies(@Query("base") baseCurrencyName: String): Single<RateListResponse>
}
