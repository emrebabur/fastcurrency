package com.emrebabur.fastcurrency.converter.view

import com.emrebabur.fastcurrency.converter.ConverterAutomaton
import com.emrebabur.fastcurrency.converter.ConverterAutomaton.Wish
import java.math.BigDecimal

object EventToWishTransformer : (ConverterEvent) -> ConverterAutomaton.Wish {
    override fun invoke(from: ConverterEvent): ConverterAutomaton.Wish =
        when (from) {
            is ConverterEvent.ItemClicked -> Wish.ChangeBaseCurrency(from.name)
            is ConverterEvent.EditTextChanged -> Wish.ChangeBaseAmount(BigDecimal(from.text))
        }
}
