package com.emrebabur.fastcurrency.converter.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.emrebabur.fastcurrency.R
import com.emrebabur.fastcurrency.application.clicks
import com.emrebabur.fastcurrency.application.textChanges
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


class BaseCurrencyViewModelViewHolder(
    view: View,
    private val eventRelay: Relay<ConverterEvent>
) : RecyclerView.ViewHolder(view) {

    private val nameTextView =
        view.findViewById<TextView>(R.id.converter_CurrencyNameTextView)
    private val amountEditText =
        view.findViewById<EditText>(R.id.converter_AmountEditText)

    var model: CurrencyViewModel? = null
        set(value) {
            field = value
            nameTextView.text = value?.name
            amountEditText.setText(value?.total.toString())
            amountEditText.setSelection(amountEditText.text.length)

            amountEditText.requestFocus()
        }

    init {
        Observable.merge(
            itemView.clicks()
                .flatMap {
                    if (model != null) {
                        Observable.just(ConverterEvent.ItemClicked(model!!.name))
                    } else {
                        Observable.empty()
                    }
                },
            amountEditText.textChanges()
                .debounce(100, TimeUnit.MILLISECONDS)
                .flatMap {
                    when {
                        it.isEmpty() -> Observable.just(amountEditText.hint.toString())
                        it.isBlank() -> Observable.empty()
                        it[0] == '.' -> Observable.just("0$it")
                        else -> Observable.just(it.toString())
                    }
                }
                .map { ConverterEvent.EditTextChanged(it) }
        )
            .subscribe { eventRelay.accept(it) }
    }
}
