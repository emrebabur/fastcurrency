package com.emrebabur.fastcurrency.converter.model

import java.math.BigDecimal

data class RateListResponse(
    val base: String,
    val rates: Map<String, BigDecimal>
)
