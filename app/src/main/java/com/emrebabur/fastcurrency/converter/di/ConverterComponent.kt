package com.emrebabur.fastcurrency.converter.di

import com.emrebabur.fastcurrency.application.AppComponent
import com.emrebabur.fastcurrency.converter.view.ConverterViewModel
import dagger.Component

@ConverterScope
@Component(
    modules = [ConverterModule::class],
    dependencies = [AppComponent::class]
)
interface ConverterComponent {

    fun inject(viewModel: ConverterViewModel)
}
