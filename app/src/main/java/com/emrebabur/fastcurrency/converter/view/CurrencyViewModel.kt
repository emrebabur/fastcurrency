package com.emrebabur.fastcurrency.converter.view

data class CurrencyViewModel(
    val name: String,
    val total: String
)
