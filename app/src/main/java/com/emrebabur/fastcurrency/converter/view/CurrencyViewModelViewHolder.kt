package com.emrebabur.fastcurrency.converter.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.emrebabur.fastcurrency.R
import com.emrebabur.fastcurrency.application.clicks
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable


class CurrencyViewModelViewHolder(
    view: View,
    private val eventRelay: Relay<ConverterEvent>
) : RecyclerView.ViewHolder(view) {

    private val nameTextView =
        view.findViewById<TextView>(R.id.converter_CurrencyNameTextView)
    private val amountTextView =
        view.findViewById<TextView>(R.id.converter_AmountTextView)

    var model: CurrencyViewModel? = null
        set(value) {
            field = value
            nameTextView.text = value?.name
            amountTextView.text = value?.total.toString()
        }

    init {
        itemView.clicks()
            .flatMap {
                if (model != null) {
                    Observable.just(ConverterEvent.ItemClicked(model!!.name))
                } else {
                    Observable.empty()
                }
            }
            .subscribe { eventRelay.accept(it) }
    }
}
