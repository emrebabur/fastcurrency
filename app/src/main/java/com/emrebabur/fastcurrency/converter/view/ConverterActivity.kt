package com.emrebabur.fastcurrency.converter.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.widget.ProgressBar
import com.emrebabur.fastcurrency.R
import com.emrebabur.fastcurrency.application.plusAssign
import com.emrebabur.fastcurrency.application.wrap
import com.emrebabur.fastcurrency.converter.BASE_CURRENCY_NAME
import com.emrebabur.fastcurrency.converter.ConverterAutomaton.Wish
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class ConverterActivity : AppCompatActivity() {
    private val disposable = CompositeDisposable()
    private lateinit var viewBinder: ConverterViewBinder
    private lateinit var viewModel: ConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_conversion)
        val recyclerView = findViewById<RecyclerView>(R.id.converter_RecyclerView)
        val loadingView = findViewById<ProgressBar>(R.id.converter_LoadingView)
        viewBinder = ConverterViewBinder(recyclerView, loadingView)
        viewModel = ViewModelProviders.of(this).get(ConverterViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        disposable += viewModel.wrap()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(viewBinder)

        disposable += viewBinder.wrap()
            .map(EventToWishTransformer)
            .subscribe(viewModel)

        viewModel.accept(Wish.Init(BASE_CURRENCY_NAME))
    }

    override fun onStop() {
        disposable.clear()
        super.onStop()
    }
}

