package com.emrebabur.fastcurrency.converter

import com.emrebabur.fastcurrency.automaton.Automaton
import java.math.BigDecimal

interface ConverterAutomaton : Automaton<ConverterAutomaton.State, ConverterAutomaton.Wish> {

    sealed class Wish {
        data class Init(val baseCurrencyName: String) : Wish()
        data class ChangeBaseAmount(val amount: BigDecimal) : Wish()
        data class ChangeBaseCurrency(val currencyName: String) : Wish()
    }

    sealed class Effect {
        data class RatesUpdated(val baseCurrencyName: String, val rates: Map<String, BigDecimal>) :
            Effect()

        data class BaseAmountChanged(val amount: BigDecimal) : Effect()
        data class BaseCurrencyChanged(val name: String) : Effect()
    }

    sealed class State {
        object Loading : State()
        data class Displaying(
            val shouldScrollTop: Boolean,
            val baseCurrency: Currency,
            val currencies: List<Currency>
        ) : State()
    }
}

