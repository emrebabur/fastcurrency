package com.emrebabur.fastcurrency.converter

import java.math.BigDecimal

data class Currency(
    val name: String,
    val rate: BigDecimal,
    val amount: BigDecimal
)
