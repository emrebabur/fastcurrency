package com.emrebabur.fastcurrency.converter.view

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.emrebabur.fastcurrency.application.FastCurrencyApp
import com.emrebabur.fastcurrency.application.wrap
import com.emrebabur.fastcurrency.converter.ConverterAutomaton
import com.emrebabur.fastcurrency.converter.di.DaggerConverterComponent
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.functions.Consumer
import javax.inject.Inject

class ConverterViewModel(application: Application) : AndroidViewModel(application),
    Consumer<ConverterAutomaton.Wish>,
    ObservableSource<ConverterViewModel.ViewModel> {

    @Inject
    protected lateinit var converterAutomaton: ConverterAutomaton

    init {
        val component = DaggerConverterComponent.builder()
            .appComponent(getApplication<FastCurrencyApp>().appComponent)
            .build()
        component.inject(this)
    }

    override fun onCleared() {
        converterAutomaton.dispose()
        super.onCleared()
    }

    override fun accept(wish: ConverterAutomaton.Wish) {
        converterAutomaton.accept(wish)
    }

    override fun subscribe(observer: Observer<in ViewModel>) {
        converterAutomaton.wrap()
            .map(StateToViewModelTransformer)
            .subscribe(observer)
    }

    sealed class ViewModel {
        object Loading : ViewModel()
        data class Displaying(
            val shouldScrollTop: Boolean,
            val currencies: List<CurrencyViewModel>
        ) : ViewModel()
    }
}
