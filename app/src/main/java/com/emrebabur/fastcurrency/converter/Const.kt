package com.emrebabur.fastcurrency.converter

import java.math.RoundingMode

internal const val BASE_CURRENCY_NAME = "EUR"
internal const val CURRENCY_UPDATE_INTERVAL_MS = 1000L
internal const val CURRENCY_UPDATE_TIMEOUT_MS = 1000L
internal const val CURRENCY_DECIMAL_SCALE = 5
internal val CURRENCY_ROUNDING_MODE = RoundingMode.HALF_EVEN
internal const val DEFAULT_BASE_CURRENCY_AMOUNT = 1
