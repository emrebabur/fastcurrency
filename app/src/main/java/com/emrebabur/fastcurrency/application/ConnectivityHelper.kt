package com.emrebabur.fastcurrency.application

import android.content.Context
import android.net.ConnectivityManager

interface ConnectivityHelper {
    fun isConnected(): Boolean
}

class BasicConnectivityHelper(private val context: Context) : ConnectivityHelper {

    private val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    override fun isConnected(): Boolean = cm.activeNetworkInfo != null
}
