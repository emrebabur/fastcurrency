package com.emrebabur.fastcurrency.application

import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class]
)
interface AppComponent {

    fun retrofitBuilder(): Retrofit.Builder
    fun connectivityHelper(): ConnectivityHelper
}
